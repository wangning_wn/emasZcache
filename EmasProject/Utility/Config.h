//
//  FMConfig.h
//  Flame
//
//  Created by WN on 16/11/28.
//  Copyright © 2016年 JOE. All rights reserved.
//

#ifndef FMConfig_h
#define FMConfig_h

#define  Request_Time_Out 30

#define ScreenFrame [UIScreen mainScreen].bounds
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height


#define iPhoneX (ScreenHeight == 812)

#define TabbarHeight (iPhoneX ? 83 : 49)

#define StatusBarHeight  [UIApplication sharedApplication].statusBarFrame.size.height

#define NavigationBarHeight (iPhoneX ? 88 : StatusBarHeight+44)


#define scaleW [UIScreen mainScreen].bounds.size.width/320
#define scaleH [UIScreen mainScreen].bounds.size.height/568


//根据屏幕判断设备
#define is_iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define is_iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define is_iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(375, 667), [UIScreen mainScreen].bounds.size) : NO)
#define is_iPhone6P ([UIScreen instancesRespondToSelector:@selector(currentMode)] ?  CGSizeEqualToSize(CGSizeMake(414, 736), [UIScreen mainScreen].bounds.size) : NO)


//是否ios8运行环境
#ifndef isIOS8Later
#define isIOS8Later ([[[UIDevice currentDevice] systemVersion] floatValue] >=8.0)
#endif

//是否ios9运行环境
#ifndef isIOS9Later
#define isIOS9Later ([[[UIDevice currentDevice] systemVersion] floatValue] >=9.0)
#endif

//是否ios10运行环境
#ifndef isIOS10Later
#define isIOS10Later ([[[UIDevice currentDevice] systemVersion] floatValue] >=10.0)
#endif


// 获得RGB颜色
#define FMRGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define FM_TitleColor FMRGBColor(142, 197, 8)



#define DeviceTokenParam @"DeviceTokenParam"

#define PassLicenseStr @"Wm91Tmc5dXZEYi9UNGdLMzhOTWZuMlhOeWVWM1Vwd0tiNm5lQUZUUXdxRzgyMWN5ZHpwclNMUFFIUWhmS09NWkZrTS9qRi9xNVNZaGNDYXlRaXZzbkk1aEhEcTVsUTZnTzZDUFEyOG9MU3ZpOW9vSjdjL0NSR0tzOEhQTXIrYlkzc2szVWJ4VWVTMmttREhDWUU3bno0anhWZURGNWRtTDVKcUVkaklNMXVvPXsiaWQiOjAsInR5cGUiOiJwcm9kdWN0IiwicGFja2FnZSI6WyJjbi5taWNyb2RvbmUuZ20uUGFzc0d1YXJkRGVtbyJdLCJhcHBseW5hbWUiOlsiUGFzc0d1YXJkIl0sInBsYXRmb3JtIjoxfQ=="


//  通知

#define SetGestureSuccess @"SetGestureSuccess"
#define ModifyGestureSuccess @"ModifyGestureSuccess"
#define GestureLoginSuccess @"GestureLoginSuccess"
#define TouchIdLoginSuccess @"TouchIdLoginSuccess"
#define weixinShareAppId @"wx4dfc6740b51a51ba"
#define weixinShareSecret @"b7a21ef064c4001453ff4398f1e77c45"


// keychain
#define FDUserName @"FDUserName"

//
#define FDConfigHeadURL @"FDConfigHeadURL"
#define FDConfigPartHeadURL @"FDConfigPartHeadURL"

#endif /* FMConfig_h */
