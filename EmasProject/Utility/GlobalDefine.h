//
//  GlobalDefine.h
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#ifndef GlobalDefine_h
#define GlobalDefine_h

//渠道号组成规则（渠道ID@应用英文名_应用os_版本号）
#define APPKey @"20000165"
#define APPSecret @"c643ec2002b819fd7f54cbc534893b25"
#define Channel @"1001@Test_iOS_1.0.0"
#define Identify @"20000165@iphoneos"
#define AppVersion @"1.0.0"

#define PackageZipPrefix @"http://publish-poc.emas-ha.cn/eweex/"
#define ACCSHostUrl @"emaspoc-acs.emas-ha.cn"
#define WapAPIURL @"emaspoc-aserver.emas-ha.cn"

#endif /* GlobalDefine_h */
