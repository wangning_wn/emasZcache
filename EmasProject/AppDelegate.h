//
//  AppDelegate.h
//  EmasProject
//
//  Created by 汪宁 on 2018/5/10.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

