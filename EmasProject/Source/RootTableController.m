//
//  RootTableController.m
//  CBBCpoc
//
//  Created by 汪宁 on 2018/4/19.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import "RootTableController.h"
#import "HomeController.h"
#import "SecondController.h"
#import "ThirdController.h"
#import "FourthController.h"
#import "HFXViewController.h"
@interface RootTableController ()

@end

@implementation RootTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HFXViewController *v1 = [[HFXViewController alloc]init];
    //v1.url =[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"home" ofType:@"js" inDirectory:@""]];
//    v1.url = [NSURL URLWithString:@"http://publish-poc.emas-ha.cn/app/test001-test/mine"];
//    v1.tabBarItem.image = [UIImage imageNamed:@"h5_tab1_normal"];
//    v1.tabBarItem.selectedImage = [UIImage imageNamed:@"h5_tab1_selected"];
//
//    v1.tabBarItem.title = @"首页";

     SecondController *v2 = [SecondController new];
    // v2.url =[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"addressBook" ofType:@"js" inDirectory:@""]];
    v2.url = [NSURL URLWithString:@"http://publish-poc.emas-ha.cn/app/test001-test/home"];
     v2.tabBarItem.image = [UIImage imageNamed:@"h5_tab2_normal"];
    v2.tabBarItem.selectedImage = [UIImage imageNamed:@"h5_tab2_selected"];
    v2.tabBarItem.title = @"金融";
    
    ThirdController *v3 = [ThirdController new];
    //v3.url =[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"approval" ofType:@"js" inDirectory:@""]];
    v3.url = [NSURL URLWithString:@"http://publish-poc.emas-ha.cn/app/test001-test/testHa_ios.js"];
    v3.tabBarItem.image = [UIImage imageNamed:@"h5_tab3_normal"];
    v3.tabBarItem.selectedImage = [UIImage imageNamed:@"h5_tab3_selected"];
    
    v3.tabBarItem.title = @"生活";
    
    FourthController *v4 = [FourthController new];
    //v4.url =[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"personalCenter" ofType:@"js" inDirectory:@""]];
    v4.url = [NSURL URLWithString:@"http://publish-poc.emas-ha.cn/app/test001-test/jserror.js"];
    v4.tabBarItem.selectedImage = [UIImage imageNamed:@"h5_tab4_selected"];
    v4.tabBarItem.image = [UIImage imageNamed:@"h5_tab4_normal"];
    v4.tabBarItem.title = @"活动";
    
    NSArray *array = @[v1,v2,v3,v4];
    
    self.viewControllers = array;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
