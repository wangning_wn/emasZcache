// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(1)
)

/* script */
__vue_exports__ = __webpack_require__(2)

/* template */
var __vue_template__ = __webpack_require__(3)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA\\src\\assets\\views\\addressBook.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-644d07cc"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = {
  "addressBook": {
    "marginBottom": "15"
  },
  "header": {
    "paddingTop": "68",
    "paddingBottom": "20",
    "backgroundColor": "#fbfbfb"
  },
  "title": {
    "fontSize": "34",
    "fontWeight": "500",
    "textAlign": "center"
  },
  "add": {
    "width": "40",
    "height": "40",
    "position": "absolute",
    "right": "32",
    "bottom": "20"
  },
  "search": {
    "paddingTop": "16",
    "paddingRight": "16",
    "paddingBottom": "16",
    "paddingLeft": "16",
    "backgroundColor": "rgb(244,244,244)",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "searchInput": {
    "width": "718",
    "height": "64",
    "backgroundColor": "rgb(255,255,255)",
    "borderRadius": "10",
    "textAlign": "center",
    "color": "rgb(153,153,153)",
    "fontSize": "26"
  },
  "menu": {
    "flexDirection": "row"
  },
  "menuCell": {
    "justifyContent": "center",
    "alignItems": "center",
    "width": "250",
    "height": "146"
  },
  "cellImg": {
    "marginTop": "24",
    "width": "48",
    "height": "48"
  },
  "cellMsg": {
    "fontSize": "26",
    "marginTop": "24",
    "marginBottom": "24",
    "textAlign": "center"
  },
  "lightBar": {
    "width": "56",
    "height": "6",
    "position": "absolute",
    "bottom": 0,
    "left": "97"
  },
  "session": {
    "backgroundColor": "rgba(0,0,0,0)"
  },
  "searchAside": {
    "position": "fixed",
    "right": "20",
    "top": "100",
    "width": "60"
  },
  "searchIcon": {
    "fontSize": "20",
    "paddingTop": "1",
    "paddingBottom": "1",
    "textAlign": "center"
  },
  "txtIcon": {
    "position": "fixed",
    "top": "300",
    "right": "375",
    "backgroundColor": "#cccccc",
    "width": "60",
    "height": "60",
    "borderRadius": "30",
    "fontSize": "30",
    "textAlign": "center",
    "paddingTop": "10",
    "paddingBottom": "10"
  },
  "addressTitle": {
    "height": "300"
  }
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
var dom = weex.requireModule('dom');

exports.default = {
  data: function data() {
    return {
      cellImgs: [{ // 菜单栏图标和文字列表
        img: '/src/assets/imgs/address.png',
        msg: '常用联系人'
      }, {
        img: '/src/assets/imgs/group.png',
        msg: '我的群组'
      }, {
        img: '/src/assets/imgs/team.png',
        msg: '组织架构'
      }],
      msg: '常用联系人', // 点击状态显示长条
      // 侧边栏搜索字母列表
      searchIcons: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
      txtIcon: ''
    };
  },
  mounted: function mounted() {
    // 获取设备视口宽高
    // var mm = ''
    // const result = dom.getComponentRect(this.$refs.searchAside, option => {
    //   mm = option
    // })
    // modal.toast({
    //   message: mm.size + ' sadasdas',
    //   duration: 3
    // })
  },

  methods: {
    // 搜索信息
    searchAddressBook: function searchAddressBook(event) {
      modal.toast({
        message: event.value, // 获取输入的搜索值
        duration: 3
      });
    },

    // 菜单栏切换
    handleJump: function handleJump(msg) {
      this.msg = msg;
    },

    // 点击侧边栏搜索字母滚动到指定位置
    scrollTo: function scrollTo(ref) {
      this.txtIcon = ref;
      var el = this.$refs[ref][0];
      dom.scrollToElement(el, {});
    }
  }
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["addressBook"]
  }, [_c('div', {
    ref: "top",
    staticClass: ["header"]
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("通讯录")]), _c('image', {
    staticClass: ["add"],
    attrs: {
      "src": "/src/assets/imgs/add.png"
    }
  })]), _c('div', {
    staticClass: ["search"]
  }, [_c('input', {
    staticClass: ["searchInput"],
    attrs: {
      "type": "text",
      "placeholder": "全行搜索（用户名、手机号、短号、座机）"
    },
    on: {
      "input": _vm.searchAddressBook
    }
  })]), _c('div', {
    staticClass: ["menu"]
  }, _vm._l((_vm.cellImgs), function(cellImg) {
    return _c('div', {
      key: cellImg.msg,
      staticClass: ["menuCell"],
      on: {
        "click": function($event) {
          _vm.handleJump(cellImg.msg)
        }
      }
    }, [_c('image', {
      staticClass: ["cellImg"],
      attrs: {
        "src": cellImg.img
      }
    }), _c('text', {
      staticClass: ["cellMsg"]
    }, [_vm._v(_vm._s(cellImg.msg))]), _c('div', {
      staticClass: ["lightBar"],
      style: {
        'backgroundColor': _vm.msg === cellImg.msg ? '#00a4ea' : ''
      }
    })])
  })), _c('div', {
    staticClass: ["session"]
  }, [_c('div', {
    ref: "searchAside",
    staticClass: ["searchAside"]
  }, [_vm._l((_vm.searchIcons), function(searchIcon) {
    return _c('text', {
      key: searchIcon,
      staticClass: ["searchIcon"],
      on: {
        "click": function($event) {
          _vm.scrollTo(searchIcon)
        }
      }
    }, [_vm._v(_vm._s(searchIcon))])
  }), _c('text', {
    staticClass: ["txtIcon"]
  }, [_vm._v(_vm._s(_vm.txtIcon))])], 2), _c('div', {
    staticClass: ["address"]
  }, _vm._l((_vm.searchIcons), function(searchIcon) {
    return _c('div', {
      key: searchIcon,
      ref: searchIcon,
      refInFor: true,
      staticClass: ["addressTitle"]
    }, [_c('text', {
      staticClass: ["addressList"]
    }, [_vm._v(_vm._s(searchIcon))])])
  }))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })
/******/ ]);