//
//  EMASService.h
//  MOAProject
//
//  Created by 万浩 on 2018/5/14.
//  Copyright © 2018年 方克东的mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMASService : NSObject

+ (EMASService *)shareInstance;

- (NSString *)appkey;
- (NSString *)appSecret;
- (NSString *)getAppVersion;
- (NSString *)ACCSDomain;
- (NSDictionary *)IPStrategy;
- (NSString *)HAServiceID;
- (NSString *)MTOPDomain;
- (NSString *)ChannelID;
- (NSString *)ZCacheURL;
- (NSString *)HAOSSBucketName;
- (NSString *)HAUniversalHost;
- (NSString *)HATimestampHost;
- (NSString *)HARSAPublicKey;
- (NSString *)HotfixServerURL;
- (BOOL)useHTTP;

@end
