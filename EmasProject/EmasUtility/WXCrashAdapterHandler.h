//
//  WXCrashAdapterHandler.h
//  Core
//
//  Created by EMAS on 2017/11/16.
//  Copyright © 2017年 taobao. All rights reserved.
//

#ifndef WXCrashAdapterHandler_h
#define WXCrashAdapterHandler_h

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>

@interface WXCrashAdapterHandler : NSObject<WXJSExceptionProtocol>

@end

#endif /* WXCrashAdapterHandler_h */
