//
//  AnalyticInit.h
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AliHAAdapter4poc/AliHAAdapter.h>
#import <AliHASecurity/AliHASecurity.h>
#import <TRemoteDebugger/TRDManagerService.h>
#import <TBRest/TBRestSendService.h>
#import <TBCrashReporter/TBCrashReporterMonitor.h>
@interface AnalyticInit : NSObject
- (void)initAnalytice;
@end
