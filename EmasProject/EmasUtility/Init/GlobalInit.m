//
//  GlobalInit.m
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#import "GlobalInit.h"
@implementation GlobalInit

- (void)initGlobalSDK{
    
    // UT初始化部分
    [[UTAnalytics getInstance] turnOffCrashHandler];
    [[UTAnalytics getInstance] turnOnDebug]; // 打开调试日志
    //scheme指定了埋点上报组件的请求协议，可以是https或者http host MTOP 需要一致
    [[UTAnalytics getInstance] setTimestampHost:@"emaspoc-aserver.emas-ha.cn" scheme:@"http"];
    [[UTAnalytics getInstance] setAppKey:APPKey secret:APPSecret];
    [[UTAnalytics getInstance] setChannel:Channel];
    [[UTAnalytics getInstance] setAppVersion:AppVersion];
    [AppMonitor disableSample]; // 调试使用，上报不采样，建议正式发布版本不要这么做
    // 网络库初始化部分
    [NWNetworkConfiguration setEnvironment:release];
    NWNetworkConfiguration *configuration = [NWNetworkConfiguration shareInstance];
    [configuration setIsUseSecurityGuard:NO];
    [configuration setAppkey:APPKey];
    [configuration setAppSecret:APPSecret];
    [configuration setIsEnableAMDC:NO];
    [NetworkDemote shareInstance].canInitWithRequest = NO;
    setNWLogLevel(NET_LOG_DEBUG); // 打开调试日志
}

@end
