//
//  AnalyticInit.m
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#import "AnalyticInit.h"
#import <MtopSDK/MtopSDK.h>
#import <MtopCore/MtopService.h>
#import "WeexModel.h"
#import "WXImgLoaderDefaultImpl.h"
#import "WXResourceRequestHandlerDemoImpl.h"
#import "WXAppMonitorHandler.h"
#import "WXEventModule.h"
#import "WXCrashAdapterHandler.h"
#import "WXCrashReporter.h"

@implementation AnalyticInit

- (void)initAnalytice{

    
    // MTOP初始化部分。网关初始化部分  这个部分weex需要 高可用需要吗？
    TBSDKConfiguration *config = [TBSDKConfiguration shareInstanceDisableDeviceID:YES andSwitchOffServerTime:YES];
    config.environment = TBSDKEnvironmentRelease;
    config.safeSecret = NO;
    config.appKey = APPKey;
    config.appSecret = APPSecret;
    config.wapAPIURL = WapAPIURL;//设置全局自定义域名
    config.wapTTID = @"1001@Test_iOS_1.0.0"; //渠道ID
    openSDKSwitchLog(YES); // 打开调试日志
    //config.enableHttps = NO; //若有降级到HTTP抓包需求，请设置为NO,否则不要设置。
    [WXSDKEngine initSDKEnvironment];
    
    // 数据上报--必选
    [WXSDKEngine registerHandler:[WXAppMonitorHandler new] withProtocol:@protocol(WXAppMonitorProtocol)];

    
    // zcache 必选
    [WXSDKEngine registerHandler:[WXResourceRequestHandlerDemoImpl new] withProtocol:@protocol(WXResourceRequestHandler)];
    [ZCache defaultCommonConfig].packageZipPrefix = PackageZipPrefix;
    [ZCache setDebugMode:YES]; // 可选
    [ZCache setupWithMtop];
    // 图片下载
    [WXSDKEngine registerHandler:[WXImgLoaderDefaultImpl new] withProtocol:@protocol(WXImgLoaderProtocol)];
    // 事件调用，可选
    [WXSDKEngine registerModule:@"event" withClass:[WeexModel class]];
    [WXSDKEngine registerModule:@"haTest" withClass:[WXEventModule class]];// 这个是测试crash的
    
    // 事件调用，可选 两种事件调用有什么区别
    [WXSDKEngine registerHandler:[WXEventModule new] withProtocol:@protocol(WXEventModuleProtocol)];

    // JSError监控--必选
    [WXSDKEngine registerHandler:[WXCrashAdapterHandler new] withProtocol:@protocol(WXJSExceptionProtocol)];
    // JSCrash监控--必选
    [[TBCrashReporterMonitor sharedMonitor] registerCrashLogMonitor:[[WXCrashReporter alloc] init]];

    
    NSString *rasPublicKey = @"";
    if (rasPublicKey.length > 0)
    {
        [[AliHASecurity sharedInstance] initWithRSAPublicKey:rasPublicKey];
    }
    [AliHAAdapter initWithAppKey:APPKey appVersion:AppVersion channel:Channel plugins:nil nick:@"emas-ha"];
    [AliHAAdapter configOSS:@"ossBucketName"];
    [AliHAAdapter setupAccsChannel:@"ACCSDomain" serviceId:@"emas-ha"];
    [AliHAAdapter setupRemoteDebugRPCChannel:@"HAUniversalHost" scheme:@"kHTTPSProtocol"]; //scheme指定了埋点上报组件的请求协议，可以是https或者http
    TBRestConfiguration *restConfiguration = [[TBRestConfiguration alloc] init];
    restConfiguration.appkey = APPKey;
    restConfiguration.appVersion = AppVersion;
    restConfiguration.channel = @"ChannelID";
    restConfiguration.usernick = @"usernick";
    restConfiguration.dataUploadHost = @"emaspoc-adash.emas-ha.cn";
    //默认请求协议为https，若需要降级到HTTP协议下抓包，可以指定dataUploadScheme为http
    restConfiguration.dataUploadScheme = @"http";
    [[TBRestSendService shareInstance] configBasicParamWithTBConfiguration:restConfiguration];
    
    
    
    //灰度发布功能
//    NSString * logicIdentifier = [NSString stringWithFormat:@"%@@%@",[[EMASService shareInstance] appkey],[self isDeviceIphone]?@"iPhone":@"iPad"];
//    [[DynamicConfigurationAdaptorManager sharedInstance] setUpWithMaxUpateTimesPerDay:10 AndIdentifier:logicIdentifier];

}

@end
