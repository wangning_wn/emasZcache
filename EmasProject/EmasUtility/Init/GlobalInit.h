//
//  GlobalInit.h
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UT/UTAnalytics.h>
#import <UT/AppMonitor.h>
#import <NetworkSDK/NetworkCore/NWNetworkConfiguration.h>
#import <NetworkSDK/NetworkCore/NetworkDemote.h>
#import <NetworkSDK/NetworkCore/NWuserLoger.h>

@interface GlobalInit : NSObject
- (void)initGlobalSDK;
@end
