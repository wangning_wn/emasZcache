//
//  AccesInit.h
//  emasSDKAccess
//
//  Created by 汪宁 on 2018/5/3.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TBAccsSDK/TBAccsManager.h>
@interface AccesInit : NSObject
- (void)initAccessSDK;
@end
