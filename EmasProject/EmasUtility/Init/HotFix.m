//
//  HotFix.m
//  EmasProject
//
//  Created by 汪宁 on 2018/5/16.
//  Copyright © 2018年 WN. All rights reserved.
//

#import "HotFix.h"
#import <AlicloudHotFixDebugEmas/AlicloudHotFixDebugServiceEmas.h>
#import <AlicloudHotFixEmas/AlicloudHotFixServiceEmas.h>
@interface HotFix ()
@property (nonatomic, strong)AlicloudHotFixServiceEmas * hotfixService;
@end

@implementation HotFix

- (void)initHotFix{
    _hotfixService = [AlicloudHotFixServiceEmas sharedInstance];
    [_hotfixService setLogEnabled:YES];
    [_hotfixService setAppVersion:@"1"];
    [_hotfixService initWithAppId:APPKey appSecret:APPSecret callback:^(BOOL res, id data, NSError *error) {
        if (res) {
            NSLog(@"HotFix SDK init success.");
        }else{
            NSLog(@"HotFix SDK init failed, error: %@", error);
        }
        
    }];

    
    // 从服务端加载补丁
    [_hotfixService loadPatch:^(BOOL res, id data, NSError *error) {
        if (res) {
            NSLog(@"Load patch success.");
        } else {
            NSLog(@"Load patch failed, error: %@", error);
        }
    }];

    
}

@end

