/*
 ZCacheCommonConfig.h
 
 Created by WindVane.
 Copyright (c) 2017年 阿里巴巴-淘宝技术部. All rights reserved.
 */

#import "ZCConfigModel.h"
#import <Foundation/Foundation.h>

/**
 预加载的通用配置。
 */
@interface ZCacheCommonConfig : ZCConfigModel

/**
 配置更新的时间间隔（毫秒），默认为 5 分钟。
 */
@property (nonatomic, assign) NSInteger configUpdateInterval;

/**
 执行异常恢复的间隔（毫秒），默认为 5 天。
 */
@property (nonatomic, assign) NSInteger recoveryInterval;

/**
 预加载的 zip 前缀。
 */
@property (nonatomic, copy, nullable) NSString * packageZipPrefix;

/**
 预加载的预览 zip 前缀。
 */
@property (nonatomic, copy, nullable) NSString * packageZipPreviewPrefix;

/**
 预加载的安全校验率，键为 AppName，值为安全校验率（double [0 - 1]）。
 */
@property (nonatomic, copy, nullable) NSDictionary * verifySampleRate;

/**
 预加载单次下载个数限制。
 */
@property (nonatomic, assign) NSInteger packageDownloadLimit;

/**
 预加载本地最多个数限制。
 */
@property (nonatomic, assign) NSUInteger packageMaxAppCount;

/**
 预加载 App 优先级的权重。
 */
@property (nonatomic, assign) double packagePriorityWeight;

/**
 预加载统计的间隔。
 */
@property (nonatomic, assign) NSInteger packageAccessInterval;

/**
 预加载执行清理的间隔。
 */
@property (nonatomic, assign) NSInteger packageRemoveInterval;

/**
 个性化预加载的配置前缀。
 */
@property (nonatomic, copy, nullable) NSString * customsConfigPrefix;

/**
 个性化预加载包直接查询更新的个数限制。
 */
@property (nonatomic, assign) NSUInteger customsDirectQueryLimit;

/**
 是否检查淘汰。
 */
@property (nonatomic, assign) BOOL isCheckCleanup;

/**
 是否开启预加载解 Combo 功能。
 */
@property (nonatomic, assign) BOOL isOpenCombo;

/**
 是否自动注册个性化预加载包。
 */
@property (nonatomic, assign) BOOL isAutoRegisterApp;

@end
