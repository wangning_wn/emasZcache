/*
 ZCConfigModel.h
 
 Created by WindVane.
 Copyright (c) 2017年 阿里巴巴-淘宝技术部. All rights reserved.
 */

#import <Foundation/Foundation.h>

// 预加载配置的加载类型。
typedef NS_ENUM(NSInteger, ZCConfigLoadType) {
	ZCConfigLoadTypeFull = -1,       // 全量配置。
	ZCConfigLoadTypeIncremental = 0, // 增量配置。
};

/**
 预加载的配置模型。
 */
@interface ZCConfigModel : NSObject

/**
 配置的版本。
 */
@property (nonatomic, copy, readonly, nonnull) NSString * version;

/**
 配置的类型。
 */
@property (nonatomic, assign, readonly) ZCConfigLoadType type;

/**
 使用指定的配置字典初始化。
 */
- (instancetype _Nonnull)initWithDictionary:(NSDictionary * _Nullable)dictionary;

@end
