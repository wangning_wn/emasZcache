//
//  OSSUploadService.h
//  RemoteDebugChannel
//
//  Created by hansong.lhs on 2017/12/16.
//  Copyright © 2017年 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AliHAProtocol/AliHAProtocol.h>
#import <TBJSONModel/TBJSONModel.h>

/**
 * OSS credintial params
 */
@interface OSSCredentialParam : TBJSONModel

@property (nonatomic, copy) NSString *ossEndpoint;
@property (nonatomic, copy) NSString *ossAccessKey;
@property (nonatomic, copy) NSString *ossSecretKey;
@property (nonatomic, copy) NSString *ossBucketName;
@property (nonatomic, copy) NSString *ossObjectPath;
@property (nonatomic, copy) NSString *ossObjectKey;
@property (nonatomic, copy) NSString *ossSecurityToken;
@property (nonatomic, strong) NSDate *expiration;

@end

/**
 * oss upload service impl
 */
@interface AliHAOSSUploadService : NSObject <AliHAUploadProtocol>

+ (AliHAOSSUploadService *)sharedInstance;

@property (nonatomic, copy) NSString *ossBucketName;

@end
