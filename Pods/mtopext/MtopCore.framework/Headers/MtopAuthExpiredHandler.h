//
//  MtopAuthExpiredHandler.h
//  mtopext
//
//  Created by jiangpan on 2017/8/24.
//  Copyright © 2017年 taobao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorHandler.h"

@interface MtopAuthExpiredHandler : NSObject <ErrorHandler>
/*!
 * Auth失效错误处理器初始化, 注册
 */
+ (instancetype) getInstance;


@end
