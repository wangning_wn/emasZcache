//    '########'########::'######:'########:'##:::'##:
//    ... ##..::##.... ##'##... ##:##.... ##:##::'##::
//    ::: ##::::##:::: ##:##:::..::##:::: ##:##:'##:::
//    ::: ##::::########:. ######::##:::: ##:#####::::
//    ::: ##::::##.... ##:..... ##:##:::: ##:##. ##:::
//    ::: ##::::##:::: ##'##::: ##:##:::: ##:##:. ##::
//    ::: ##::::########:. ######::########::##::. ##:
//    :::..::::........:::......::........::..::::..::
//
//  Created by Roger.Mu on 14-11-06.
//  Copyright (c) 2014年 Taobao.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBSDKBIZCacheObject.h"

#define kTBSDKDemoteSPDY @"kTBSDKDemoteSPDY"
#define kTBSDKDemoteURLConnection @"kTBSDKDemoteURLConnection"

@interface TBSDKDemoteManager : NSOperationQueue

@property (nonatomic, unsafe_unretained) BOOL netWorkUTOnOff;
@property (nonatomic, unsafe_unretained) long long postGzipOnoff;
@property (nonatomic, unsafe_unretained) int sslLimited;
@property (nonatomic, unsafe_unretained) BOOL unitEnable;
@property (nonatomic, weak) id nwDemote;

+ (TBSDKDemoteManager *)shareInstance;

#pragma mark -- mtopsdk switch interface functions

- (BOOL)isDemoteCache;

- (void)demoteCache;

- (void)unDemoteCache;

- (BOOL)isDemoteSSL;

- (BOOL)isDemoteWua;

- (BOOL)isDemoteBizErrorMapping;

- (int)BizErrorMappingLimit;

- (void)unSSL;

- (BOOL)isDemoteSPDY;

- (void)unDemoteSPDY;

- (void)demoteSPDY;

- (BOOL)isDemoteDNS;

- (void)unDemoteDNS;

- (void)demoteDNS;

- (void)stopHttpDNS;

#pragma mark -- mtop cache interface functions

/**
 * cache biz object change to cache dictionary
 */
- (void)setBizCache:(TBSDKBIZCacheObject *) bizCache;

/**
 *  API纬度是否需要降级业务错误映射
 *  @param apiFlag   mtop接口标识
 *  @return     yes  需要降级
 *              no   不需要降级
 */

- (BOOL)isApiNeedDemoteBizErrorMapping:(NSString *)apiFlag;

/**
 * 自定义配置文案映射
 * @param msg  需要映射的文案
 * @return     映射过后的文案
 */

- (NSString *)customSystemErrorMsg:(NSString *)msg;

@end


@protocol OrangeProtocal <NSObject>

+ (void)run;

+ (id)getConfigByGroupName:(NSString *)groupName
                       key:(NSString *)key
             defaultConfig:(id)defaultConfig
                 isDefault:(BOOL *)isDefault;

+ (NSDictionary *)getGroupConfigByGroupName:(NSString *)groupName;

@end
