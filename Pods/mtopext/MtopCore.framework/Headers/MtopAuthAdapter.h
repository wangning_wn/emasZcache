//
//  MtopAuthAdapter.h
//  mtopext
//
//  Created by jiangpan on 2017/8/23.
//  Copyright © 2017年 taobao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MtopAuthProtocol.h"

@class MtopExtRequest;

typedef enum {
    /* 授权错误 */
    AUTH_ERROR,
    
    /* 授权已经被其他线程唤起, 所以只是把该请求pending起来了 */
    AUTH_PENDINGED,
    
    /* 自己唤起了授权, 然后把该请求pending起来了 */
    AUTH_INVOKED_AUTH_AND_PENDINGED,
    
    /* 其他线程已经授权成功, 所以，神马也没做 */
    AUTH_AUTHED
    
} TRY_AUTH_RESULT;

@interface MtopAuthAdapter : NSObject <MtopAuthProtocol>

@property (nonatomic, strong, readonly) id<MtopAuthProtocol>  module;

- (void)setCustomAuthModule:(id<MtopAuthProtocol>)module;

- (TRY_AUTH_RESULT)tryAuth:(MtopExtRequest *)request withAuthParam:(AuthParamObj *)authParam;

@end

@interface AuthParamObj:NSObject

@property (nonatomic,strong) NSDictionary *authParam; // 授权参数
@property (nonatomic,copy) NSString *openAppkey; // 三方授权业务的appkey

- (instancetype)initWithAppkey:(NSString *)appkey andAuthParam:(NSDictionary *)authParam;

@end

