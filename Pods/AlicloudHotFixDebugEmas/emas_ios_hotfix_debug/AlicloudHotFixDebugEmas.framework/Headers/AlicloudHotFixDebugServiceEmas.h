//
//  AlicloudHotFixDebugService.h
//  AlicloudHotFixDebug
//
//  Created by junmo on 2017/9/25.
//  Copyright © 2017年 junmo. All rights reserved.
//

#ifndef AlicloudHotFixDebugServiceEmas_h
#define AlicloudHotFixDebugServiceEmas_h

#import <UIKit/UIKit.h>

@interface AlicloudHotFixDebugServiceEmas : NSObject

+ (void)showDebug:(UIViewController *)parentViewController;

+ (BOOL)loadLocalPachFile:(NSString *)filePath;

@end

#endif /* AlicloudHotFixDebugService_h */
