//
//  DynamicConfigurationAdaptorManager.h
//  DynamicConfigurationAdaptor
//
//  Created by haoxuan on 17/2/22.
//  Copyright © 2017年 Taobao lnc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DynamicConfigurationAdaptorManager : NSObject

- (instancetype)init NS_UNAVAILABLE;
+ (DynamicConfigurationAdaptorManager*)sharedInstance;
/*
 * 初始化
 * @para times 每天最大更新次数
 */
- (void)setUpWithMaxUpateTimesPerDay:(NSInteger)times AndIdentifier:(NSString*)identifier;

- (NSString*)identifier;

@end
